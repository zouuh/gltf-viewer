file(REMOVE_RECURSE
  "bin/shaders/diffuse_directional_light.fs.glsl"
  "bin/shaders/forward.vs.glsl"
  "bin/shaders/magenta.fs.glsl"
  "bin/shaders/normals.fs.glsl"
  "CMakeFiles/gltf-viewer.dir/src/ViewerApplication.cpp.o"
  "CMakeFiles/gltf-viewer.dir/src/main.cpp.o"
  "CMakeFiles/gltf-viewer.dir/src/tiny_gltf_impl.cpp.o"
  "CMakeFiles/gltf-viewer.dir/src/utils/cameras.cpp.o"
  "CMakeFiles/gltf-viewer.dir/src/utils/gl_debug_output.cpp.o"
  "CMakeFiles/gltf-viewer.dir/src/utils/gltf.cpp.o"
  "CMakeFiles/gltf-viewer.dir/src/utils/images.cpp.o"
  "CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/imgui.cpp.o"
  "CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/imgui_demo.cpp.o"
  "CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/imgui_draw.cpp.o"
  "CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/imgui_widgets.cpp.o"
  "CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/examples/imgui_impl_opengl3.cpp.o"
  "CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/examples/imgui_impl_glfw.cpp.o"
  "CMakeFiles/gltf-viewer.dir/third-party/glad/src/glad.c.o"
  "bin/gltf-viewer.pdb"
  "bin/gltf-viewer"
)

# Per-language clean rules from dependency scanning.
foreach(lang C CXX)
  include(CMakeFiles/gltf-viewer.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
