# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/third-party/glad/src/glad.c" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/glad/src/glad.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GLM_ENABLE_EXPERIMENTAL"
  "IMGUI_IMPL_OPENGL_LOADER_GLAD"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "third-party/glfw-3.3.1/include"
  "third-party/glm-0.9.9.7"
  "third-party/glad/include"
  "third-party/imgui-1.74"
  "third-party/imgui-1.74/examples"
  "third-party/tinygltf-bcf2ce586ee8bf2a2a816afa6bfe2f8692ba6ac2/include"
  "third-party/args-6.2.2"
  "lib/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/src/ViewerApplication.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/ViewerApplication.cpp.o"
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/src/main.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/main.cpp.o"
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/src/tiny_gltf_impl.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/tiny_gltf_impl.cpp.o"
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/src/utils/cameras.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/utils/cameras.cpp.o"
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/src/utils/gl_debug_output.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/utils/gl_debug_output.cpp.o"
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/src/utils/gltf.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/utils/gltf.cpp.o"
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/src/utils/images.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/utils/images.cpp.o"
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/third-party/imgui-1.74/examples/imgui_impl_glfw.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/examples/imgui_impl_glfw.cpp.o"
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/third-party/imgui-1.74/examples/imgui_impl_opengl3.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/examples/imgui_impl_opengl3.cpp.o"
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/third-party/imgui-1.74/imgui.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/imgui.cpp.o"
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/third-party/imgui-1.74/imgui_demo.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/imgui_demo.cpp.o"
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/third-party/imgui-1.74/imgui_draw.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/imgui_draw.cpp.o"
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/third-party/imgui-1.74/imgui_widgets.cpp" "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/imgui_widgets.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLM_ENABLE_EXPERIMENTAL"
  "IMGUI_IMPL_OPENGL_LOADER_GLAD"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "third-party/glfw-3.3.1/include"
  "third-party/glm-0.9.9.7"
  "third-party/glad/include"
  "third-party/imgui-1.74"
  "third-party/imgui-1.74/examples"
  "third-party/tinygltf-bcf2ce586ee8bf2a2a816afa6bfe2f8692ba6ac2/include"
  "third-party/args-6.2.2"
  "lib/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/6ima3/mrosenbe/Documents/gltf-viewer/gltf-viewer/third-party/glfw-3.3.1/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
