#include "cameras.hpp"
#include "glfw.hpp"

#include <iostream>

// Good reference here to map camera movements to lookAt calls
// http://learnwebgl.brown37.net/07_cameras/camera_movement.html

using namespace glm;

struct ViewFrame
{
  vec3 left;
  vec3 up;
  vec3 front;
  vec3 eye;

  ViewFrame(vec3 l, vec3 u, vec3 f, vec3 e) : left(l), up(u), front(f), eye(e)
  {
  }
};

ViewFrame fromViewToWorldMatrix(const mat4 &viewToWorldMatrix)
{
  return ViewFrame{-vec3(viewToWorldMatrix[0]), vec3(viewToWorldMatrix[1]),
      -vec3(viewToWorldMatrix[2]), vec3(viewToWorldMatrix[3])};
}

bool FirstPersonCameraController::update(float elapsedTime)
{
  if (glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_MIDDLE) &&
      !m_LeftButtonPressed) {
    m_LeftButtonPressed = true;
    glfwGetCursorPos(
        m_pWindow, &m_LastCursorPosition.x, &m_LastCursorPosition.y);
  } else if (!glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_MIDDLE) &&
             m_LeftButtonPressed) {
    m_LeftButtonPressed = false;
  }

  const auto cursorDelta = ([&]() {
    if (m_LeftButtonPressed) {
      dvec2 cursorPosition;
      glfwGetCursorPos(m_pWindow, &cursorPosition.x, &cursorPosition.y);
      const auto delta = cursorPosition - m_LastCursorPosition;
      m_LastCursorPosition = cursorPosition;
      return delta;
    }
    return dvec2(0);
  })();

  float truckLeft = 0.f;
  float pedestalUp = 0.f;
  float dollyIn = 0.f;
  float rollRightAngle = 0.f;

  if (glfwGetKey(m_pWindow, GLFW_KEY_W)) {
    dollyIn += m_fSpeed * elapsedTime;
  }

  // Truck left
  if (glfwGetKey(m_pWindow, GLFW_KEY_A)) {
    truckLeft += m_fSpeed * elapsedTime;
  }

  // Pedestal up
  if (glfwGetKey(m_pWindow, GLFW_KEY_UP)) {
    pedestalUp += m_fSpeed * elapsedTime;
  }

  // Dolly out
  if (glfwGetKey(m_pWindow, GLFW_KEY_S)) {
    dollyIn -= m_fSpeed * elapsedTime;
  }

  // Truck right
  if (glfwGetKey(m_pWindow, GLFW_KEY_D)) {
    truckLeft -= m_fSpeed * elapsedTime;
  }

  // Pedestal down
  if (glfwGetKey(m_pWindow, GLFW_KEY_DOWN)) {
    pedestalUp -= m_fSpeed * elapsedTime;
  }

  if (glfwGetKey(m_pWindow, GLFW_KEY_Q)) {
    rollRightAngle -= 0.001f;
  }
  if (glfwGetKey(m_pWindow, GLFW_KEY_E)) {
    rollRightAngle += 0.001f;
  }

  // cursor going right, so minus because we want pan left angle:
  const float panLeftAngle = -0.01f * float(cursorDelta.x);
  const float tiltDownAngle = 0.01f * float(cursorDelta.y);

  const auto hasMoved = truckLeft || pedestalUp || dollyIn || panLeftAngle ||
                        tiltDownAngle || rollRightAngle;
  if (!hasMoved) {
    return false;
  }

  m_camera.moveLocal(truckLeft, pedestalUp, dollyIn);
  m_camera.rotateLocal(rollRightAngle, tiltDownAngle, 0.f);
  m_camera.rotateWorld(panLeftAngle, m_worldUpAxis);

  return true;
}

bool TrackballCameraController::update(float elapsedTime) { 

  if(glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_MIDDLE) && !m_MiddleButtonPressed) {
    m_MiddleButtonPressed = true;
    glfwGetCursorPos(m_pWindow, &m_LastCursorPosition.x, &m_LastCursorPosition.y);
  } else if (!glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_MIDDLE) && m_MiddleButtonPressed) {
    m_MiddleButtonPressed = false;
  }

  const auto cursorDelta = ([&]() {
    if (m_MiddleButtonPressed) {
      dvec2 cursorPosition;
      glfwGetCursorPos(m_pWindow, &cursorPosition.x, &cursorPosition.y);
      const auto delta = cursorPosition - m_LastCursorPosition;
      m_LastCursorPosition = cursorPosition;
      return delta;
    }
    return dvec2(0);
  })();

  float truck = 0.f;
  float pedestal = 0.f;
  float dolly = 0.f;
  float rollRightAngle = 0.f;
  float panLeftAngle = 0.f;
  float tiltDownAngle = 0.f;

  if (glfwGetKey(m_pWindow, GLFW_KEY_LEFT_SHIFT)) {
    truck += 0.01f * float(cursorDelta.x);
    pedestal += 0.01f * float(cursorDelta.y);

    const auto hasMoved = truck || pedestal;
    if (!hasMoved) {
      return false;
    }

    m_camera.moveLocal(truck, pedestal, 0.f);
    return true;
  }

  else if (glfwGetKey(m_pWindow, GLFW_KEY_LEFT_CONTROL)) {
    dolly += 0.01f * float(cursorDelta.x);
    
    if(dolly == 0.f) {
      return false;
    }

    const auto viewVector = m_camera.center() - m_camera.eye();
    const auto len = glm::length(viewVector);

    if(glm::distance(m_camera.center(), m_camera.eye()) < 0.1f && dolly > 0.f) {
      return false;
    }
    
    const auto front = viewVector / len;
    const auto translationVector = dolly * front;

    const auto newEye = m_camera.eye() + translationVector;
    m_camera = Camera(newEye, m_camera.center(), m_worldUpAxis);
    return true;
  }

  // cursor going right, so minus because we want pan left angle:
  panLeftAngle = -0.01f * float(cursorDelta.x);
  tiltDownAngle = 0.01f * float(cursorDelta.y);

  const auto hasMoved = panLeftAngle || tiltDownAngle;
  if(!hasMoved) {
    return false;
  }

  const auto depthAxis = m_camera.eye() - m_camera.center();

  const auto horizontalAxis = m_camera.left();
  
  // const auto longitudeRotMatrix = rotate(mat4(1), tiltDownAngle, horizontalAxis);
  const auto latitudeRotMatrix = rotate(mat4(1), panLeftAngle, m_worldUpAxis);
  // const auto finalDepthAxis = vec3(latitudeRotMatrix * vec4(vec3(longitudeRotMatrix * vec4(depthAxis,0)),0));
  
  const auto rotationMatrix = rotate(latitudeRotMatrix, tiltDownAngle, horizontalAxis);
  const auto finalDepthAxis = vec3(rotationMatrix * vec4(depthAxis, 0));

  const auto newEye = m_camera.center() + finalDepthAxis;

  // Try to stop rotation before bug on vertical axis
  // const auto viewVector = m_camera.center() - newEye;
  // const auto len = glm::length(viewVector);
  // const auto front = viewVector / len;

  // if(front == glm::vec3(0.f, 0.f, 1.f) && tiltDownAngle > 0.f) {
  //   return false;
  // }

  m_camera = Camera(newEye, m_camera.center(), m_worldUpAxis);

  return true; 
}
