#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLightDir;
uniform vec3 uLightIntensity;

uniform vec4 uBaseColorFactor;

uniform float uMetallicFactor;
uniform float uRoughnessFactor;
uniform sampler2D uMetallicRoughnessTexture;

uniform sampler2D uBaseColorTexture;

out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;


vec3 LINEARtoSRGB(vec3 color) { return pow(color, vec3(INV_GAMMA)); }

vec4 SRGBtoLINEAR(vec4 srgbIn)
{
  return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

float D(float alpha, float NdotH) {
    float xi = 0.;
    if(NdotH > 0.) float xi = 1.;
    float toPow = ((NdotH * NdotH)*( (alpha * alpha) - 1. ) + 1.);
    return ((alpha * alpha) * xi) / (M_PI * (toPow * toPow));
}

float G(float alpha, float NdotL, float HdotL, float HdotV, float NdotV) {
    float xi_1 = 0;
    if(HdotL > 0) float xi_1 = 1;

    float xi_2 = 0;
    if(HdotV > 0) float xi_2 = 1;

    //finir le G = (Smith joint masking-shadowing function)
    float part1 = ( 2 * NdotL * xi_1 * HdotL ) / ( NdotL + sqrt( (alpha * alpha) + (1 - (alpha * alpha)) *  (NdotL * NdotL) ) );
    float part2 = ( 2 * NdotV * xi_2 * HdotV ) / ( NdotV + sqrt( (alpha * alpha) + (1 - (alpha * alpha)) *  (NdotV * NdotV) ) ); 
    return part1 * part2;
}

void main()
{
  vec3 N = normalize(vViewSpaceNormal);
  vec3 L = uLightDir;
  vec3 V = normalize(-vViewSpacePosition);
  vec3 H = normalize(L + V);

  vec4 baseColorFromTexture = SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
  vec4 baseColor = baseColorFromTexture * uBaseColorFactor;
  vec4 metallicRoughnessFromTexture = texture(uMetallicRoughnessTexture, vTexCoords);
  float NdotL = clamp(dot(N, L), 0., 1.);
  float NdotH = clamp(dot(N, H), 0., 1.);
  float VdotH = clamp(dot(V, H), 0., 1.);
  float VdotN = clamp(dot(V, N), 0., 1.);
  float HdotL = clamp(dot(H, L), 0., 1.);
  float HdotV = -VdotH;
  float NdotV = -VdotN;
  float LdotN = - NdotL;
  vec3 diffuse = baseColor.rgb * M_1_PI;

  const vec3 dielectricSpecular = vec3(0.04);
  const vec3 black = vec3(0);

  vec3 c_diff = mix(baseColor.rgb * (1 - dielectricSpecular), black, uMetallicFactor);

  vec3 f0 = mix(dielectricSpecular, baseColor.rgb, metallicRoughnessFromTexture.b * uMetallicFactor);

  float alpha = (metallicRoughnessFromTexture.g * uRoughnessFactor) * (metallicRoughnessFromTexture.g * uRoughnessFactor);

  float toPow1 = (1 - abs(VdotH)) * (1 - abs(VdotH));
  toPow1 *= toPow1;
  toPow1 *= (1 - abs(VdotH));
  vec3 F = f0 + (vec3(1) - f0) * toPow1;
  
  vec3 f_diffuse = (1 - F) * (1 / M_PI) * c_diff;
  vec3 f_specular = F * D(alpha, NdotH) * G(alpha, NdotL, HdotL, HdotV, NdotV) / (4 * abs(VdotN) * abs(LdotN));

  vec3 f = f_diffuse + f_specular;
  fColor = LINEARtoSRGB(f * uLightIntensity * NdotL);
}