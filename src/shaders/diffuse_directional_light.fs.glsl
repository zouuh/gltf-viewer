#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLightDir;
uniform vec3 uLightIntensity;

out vec3 fColor;

void main() {
    vec3 viewSpaceNorm = normalize(vViewSpaceNormal);
    float piInverse = 1. / 3.14;
    fColor = vec3(piInverse) * uLightIntensity * dot(viewSpaceNorm, uLightDir);
}