#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/images.hpp"
#include "utils/gltf.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>
#include "utils/gltf.hpp"

const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

bool ViewerApplication::loadGltfFile(tinygltf::Model & model){
  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;

  bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

  if (!warn.empty()) {
    printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
    printf("Failed to parse glTF\n");
    return false;
  }
  return ret;
}

std::vector<GLuint> ViewerApplication::createBufferObjects( const tinygltf::Model &model) {
  
  std::vector<GLuint> bufferObjects(model.buffers.size(), 0);

  glGenBuffers(model.buffers.size(), bufferObjects.data());
  for (size_t i = 0; i < model.buffers.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(), model.buffers[i].data.data(), 0);
  }
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return bufferObjects;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects( const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects, std::vector<VaoRange> & meshIndexToVaoRange) {
  std::vector<GLuint> vaos;

  for (size_t i= 0; i < model.meshes.size(); i++) {
    const GLuint vaoOffset = vaos.size();
    vaos.resize(vaoOffset + model.meshes[i].primitives.size());
    meshIndexToVaoRange.push_back(VaoRange{vaoOffset, model.meshes[i].primitives.size()});
    
    glGenVertexArrays(model.meshes[i].primitives.size(), &vaos[i]);
    for (size_t j=0; j < model.meshes[i].primitives.size(); j++) {
      glBindVertexArray(vaos[vaoOffset + j]);
      const auto & primitive = model.meshes[i].primitives[j];
      // POSITION
      { // I'm opening a scope because I want to reuse the variable iterator in the code for NORMAL and TEXCOORD_0
        const auto iterator = primitive.attributes.find("POSITION");
        if (iterator != end(primitive.attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; // TODO get the correct tinygltf::Accessor from model.accessors
          const auto &bufferView = model.bufferViews[accessor.bufferView]; // TODO get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
          const auto bufferIdx = bufferView.buffer; // TODO get the index of the buffer used by the bufferView (you need to use it)

          const auto bufferObject = bufferObjects[bufferIdx]; // TODO get the correct buffer object from the buffer index

          // TODO Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);
          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;// TODO Compute the total byte offset using the accessor and the buffer view
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), (const GLvoid*)byteOffset);
          // Remember size is obtained with accessor.type, type is obtained with accessor.componentType.
          // The stride is obtained in the bufferView, normalized is always GL_FALSE, and pointer is the byteOffset (don't forget the cast).
        }
      }
      // NORMAL
      { // I'm opening a scope because I want to reuse the variable iterator in the code for NORMAL and TEXCOORD_0
        const auto iterator = primitive.attributes.find("NORMAL");
        if (iterator != end(primitive.attributes)) { // If "NORMAL" has been found in the map
          // (*iterator).first is the key "NORMAL", (*iterator).second is the value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; // TODO get the correct tinygltf::Accessor from model.accessors
          const auto &bufferView = model.bufferViews[accessor.bufferView]; // TODO get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
          const auto bufferIdx = bufferView.buffer; // TODO get the index of the buffer used by the bufferView (you need to use it)

          const auto bufferObject = bufferObjects[bufferIdx]; // TODO get the correct buffer object from the buffer index

          // TODO Enable the vertex attrib array corresponding to NORMAL with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_NORMAL_IDX which has to be defined at the top of the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;// TODO Compute the total byte offset using the accessor and the buffer view
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), (const GLvoid*)byteOffset);
          // Remember size is obtained with accessor.type, type is obtained with accessor.componentType.
          // The stride is obtained in the bufferView, normalized is always GL_FALSE, and pointer is the byteOffset (don't forget the cast).
        }
      }
      // TEXCOORD_0
      { // I'm opening a scope because I want to reuse the variable iterator in the code for NORMAL and TEXCOORD_0
        const auto iterator = primitive.attributes.find("TEXCOORD_0");
        if (iterator != end(primitive.attributes)) { // If "TEXCOORD_0" has been found in the map
          // (*iterator).first is the key "TEXCOORD_0", (*iterator).second is the value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; // TODO get the correct tinygltf::Accessor from model.accessors
          const auto &bufferView = model.bufferViews[accessor.bufferView]; // TODO get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
          const auto bufferIdx = bufferView.buffer; // TODO get the index of the buffer used by the bufferView (you need to use it)

          const auto bufferObject = bufferObjects[bufferIdx]; // TODO get the correct buffer object from the buffer index

          // TODO Enable the vertex attrib array corresponding to TEXCOORD_0 with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_TEXCOORD0_IDX which has to be defined at the top of the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;// TODO Compute the total byte offset using the accessor and the buffer view
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), (const GLvoid*)byteOffset);
          // Remember size is obtained with accessor.type, type is obtained with accessor.componentType.
          // The stride is obtained in the bufferView, normalized is always GL_FALSE, and pointer is the byteOffset (don't forget the cast).
        }
      }

      if(primitive.indices >= 0) {
        const auto accessorIdx = primitive.indices;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
      }
    }
  }

  glBindVertexArray(0);

  return vaos;
}
  

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const {
  std::vector<GLuint> texObjects(model.textures.size(), 0);
  
  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);

  glGenTextures(GLsizei(model.textures.size()), texObjects.data());

  for(size_t i = 0 ; i < model.textures.size() ; i++ ){
    //std::vector<float> pixels(model.images[i].width * model.images[i].height * model.images[i].component, 0);
    const auto &texture = model.textures[i];
    int sourceId = texture.source;
    assert(sourceId >= 0);

    const auto &image = model.images[sourceId];

    auto &sampler = defaultSampler;

    if (texture.sampler >= 0) {
      sampler = model.samplers[texture.sampler];
    }
    else {
      sampler = defaultSampler;
    }

    // Bind texture object to target GL_TEXTURE_2D:
    glBindTexture(GL_TEXTURE_2D, texObjects[i]);

    // Set image data:
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width,image.height, 0,
            GL_RGBA, image.pixel_type, image.image.data());

    // Set sampling parameters:
    auto &minFilter = sampler.minFilter;
    if (minFilter == -1) {
      minFilter = GL_LINEAR;
    }

    auto &magFilter = sampler.magFilter;
    if (sampler.magFilter == -1) {
      magFilter = GL_LINEAR;
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);

    // Set wrapping parameters:
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST || sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR || sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST || sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }

    glBindTexture(GL_TEXTURE_2D, 0);
  }

  return texObjects;

}

int ViewerApplication::run()
{

    tinygltf::Model model; 
  // TODO Loading the glTF file
  loadGltfFile(model);

  glm::vec3 bboxMin, bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);

  // Loader shaders
  const auto glslProgram =
      compileProgram({m_ShadersRootPath / m_vertexShader,
          m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");

  const auto uLightIntensityLocation = glGetUniformLocation(glslProgram.glId(), "uLightIntensity");
  const auto uLightDirectionLocation = glGetUniformLocation(glslProgram.glId(), "uLightDir");

  const auto uBaseColorTexture = glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  
  const auto uBaseColorFactorLocation = glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");

  const auto uMetallicRoughnessTexture = glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
  const auto uMetallicFactor = glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto uRoughnessFactor = glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");

  const auto uEmissiveFactor = glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");
  const auto uEmissiveTexture = glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");

  const auto uOcclusionTexture = glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto uOcclusionStrength = glGetUniformLocation(glslProgram.glId(), "uOcclusionStrength");
  const auto uApplyOcclusion = glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");
  

  

  // Build projection matrix
  auto maxDistance = glm::distance(bboxMin, bboxMax); // TODO use scene bounds instead to compute this
  maxDistance = maxDistance == 0.f ? maxDistance : 100.f;
  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  // TODO Implement a new CameraController model and use it instead. Propose the
  // choice from the GUI

  // TrackballCameraController cameraController{ m_GLFWHandle.window(), 0.2f * maxDistance };
  std::unique_ptr<CameraController> cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);

  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {
    // TODO Use scene bounds to compute a better default camera
    if(bboxMin.z == bboxMax.z) {
      cameraController->setCamera(
        Camera{(bboxMin+bboxMax) * 0.5f + 2.f * glm::cross(bboxMax-bboxMin, glm::vec3(0, 1, 0)), (bboxMin + bboxMax) * 0.5f , glm::vec3(0, 1, 0)});
    }
    else {
      cameraController->setCamera(
        Camera{(bboxMin+bboxMax) * 0.5f + bboxMax-bboxMin, (bboxMin + bboxMax) * 0.5f , glm::vec3(0, 1, 0)});
    }
  }

  glm::vec3 lightIntensity(1, 1, 1);
  glm::vec3 lightDirection(1, 1, 1);

  bool lightFromCam = false;
  bool applyOcclusion = true;

  const auto textureObjects = createTextureObjects(model);

  //create white texture
  float whitePixel[] = {1,1,1,1};

  GLuint whiteTexture;
  // Generate the texture object:
  glGenTextures(1, &whiteTexture);

  // Bind texture object to target GL_TEXTURE_2D:
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  // Set image data:
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, whitePixel);
  // Set sampling parameters:
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // Set wrapping parameters:
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);

  glBindTexture(GL_TEXTURE_2D, 0);


  // TODO Creation of Buffer Objects
  std::vector<GLuint> vbo = createBufferObjects(model);
  // TODO Creation of Vertex Array Objects
  std::vector<VaoRange> meshIndexToVaoRange;
  std::vector<GLuint> vaos = createVertexArrayObjects(model, vbo, meshIndexToVaoRange);
  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  //lambda function for materials
  const auto bindMaterial = [&](const auto materialIndex) {
    // Material binding
    if(materialIndex >= 0){
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;
      auto textureObject = whiteTexture;
      auto mrTextureObject = 0u;
      auto emissiveTextureObject = 0u;
      auto occlusionTextureObject = 0u;
      auto occlusionStrength = 0u;
      if(pbrMetallicRoughness.baseColorTexture.index >= 0){
        const auto &texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];
        if(texture.source >= 0) textureObject = textureObjects[texture.source];
      }
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, textureObject);
      // By setting the uniform to 0, we tell OpenGL the texture is bound on tex unit 0:
      glUniform1i(uBaseColorTexture, 0);
      glUniform4f(uBaseColorFactorLocation,
                  (float)pbrMetallicRoughness.baseColorFactor[0],
                  (float)pbrMetallicRoughness.baseColorFactor[1],
                  (float)pbrMetallicRoughness.baseColorFactor[2],
                  (float)pbrMetallicRoughness.baseColorFactor[3]);
                                
      if(pbrMetallicRoughness.metallicRoughnessTexture.index >= 0){
        const auto &metallicRoughnessTexture = model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];
        if(metallicRoughnessTexture.source >= 0) mrTextureObject = textureObjects[metallicRoughnessTexture.source];
      }
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, mrTextureObject);
      // By setting the uniform to 1, we tell OpenGL the texture is bound on tex unit 1:
      glUniform1i(uMetallicRoughnessTexture, 1);
      glUniform1f(uMetallicFactor, (float)pbrMetallicRoughness.metallicFactor);
      glUniform1f(uRoughnessFactor, (float)pbrMetallicRoughness.roughnessFactor);

      if(material.emissiveTexture.index >= 0){
        const auto &emissiveTexture = model.textures[material.emissiveTexture.index];
        if(emissiveTexture.source >= 0) emissiveTextureObject = textureObjects[emissiveTexture.source];
      }
      glActiveTexture(GL_TEXTURE2);
      glBindTexture(GL_TEXTURE_2D, emissiveTextureObject);
      // By setting the uniform to 2, we tell OpenGL the texture is bound on tex unit 2:
      glUniform1i(uEmissiveTexture, 2);
      glUniform3f(uEmissiveFactor, (float)material.emissiveFactor[0], (float)material.emissiveFactor[1], (float)material.emissiveFactor[2]);

      if(material.occlusionTexture.index >= 0){
        const auto &occlusionTexture = model.textures[material.occlusionTexture.index];
        if(occlusionTexture.source >= 0) {
          occlusionTextureObject = textureObjects[occlusionTexture.source];
          occlusionStrength = material.occlusionTexture.strength;
        }
      }
      glActiveTexture(GL_TEXTURE3);
      glBindTexture(GL_TEXTURE_2D, occlusionTextureObject);
      // By setting the uniform to 2, we tell OpenGL the texture is bound on tex unit 2:
      glUniform1i(uOcclusionTexture, 3);
      glUniform1f(uOcclusionStrength, (float)occlusionStrength);
      if(applyOcclusion) glUniform1i(uApplyOcclusion, 1);
      else glUniform1i(uApplyOcclusion, 0);
    }
    else {
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, whiteTexture);
      // By setting the uniform to 0, we tell OpenGL the texture is bound on tex unit 0:
      glUniform1i(uBaseColorTexture, 0);
      glUniform4f(uBaseColorFactorLocation, 1, 1, 1, 1);
    }
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();

    if(uLightIntensityLocation >= 0) {
      glUniform3f(uLightIntensityLocation, lightIntensity[0], lightIntensity[1], lightIntensity[2]);
    }
    if(uLightDirectionLocation >= 0) {
      if(lightFromCam){
        glUniform3f(uLightDirectionLocation, 0, 0, 1);
      }
      else {
        const auto lightDirectionInViewSpace = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));
        glUniform3f(uLightDirectionLocation, lightDirectionInViewSpace[0], lightDirectionInViewSpace[1], lightDirectionInViewSpace[2]);
      }
    }

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          // TODO The drawNode function
          const auto node = model.nodes[nodeIdx];
          glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

          if(node.mesh >= 0) {
            glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;
            glm::mat4 modelViewProjectionMatrix = projMatrix * modelViewMatrix;
            glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelViewMatrix));

            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
            glUniformMatrix4fv(modelViewProjMatrixLocation , 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));

            const auto &mesh = model.meshes[node.mesh];
            const auto &vaoRange = meshIndexToVaoRange[node.mesh];

            for(size_t i = 0 ; i < mesh.primitives.size() ; i++){
              bindMaterial(mesh.primitives[i].material);
              const auto vao = vaos[vaoRange.begin + i];
              glBindVertexArray(vao);
              const auto &primitive = mesh.primitives[i];
              if(primitive.indices >= 0 ) {
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;

                glDrawElements(primitive.mode, (GLsizei)accessor.count, accessor.componentType, (const GLvoid*)byteOffset);
              }
              else {
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];

                glDrawArrays(primitive.mode, 0, (GLsizei)accessor.count);
              }
              glBindVertexArray(0);
            }
          }
          for(size_t i=0; i<node.children.size(); i++) {
            drawNode(node.children[i], modelMatrix);
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      // TODO Draw all nodes
      for (size_t i=0; i < model.scenes[model.defaultScene].nodes.size(); i++) {
        drawNode(model.scenes[model.defaultScene].nodes[i], glm::mat4(1));
      }
    }
  };

  if(!m_OutputPath.empty()) {
    std::vector<unsigned char> pixels;
    pixels.resize(m_nWindowWidth * m_nWindowHeight * 3);
    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), [&]() { drawScene(cameraController->getCamera()); });
  
    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());

    const auto strPath = m_OutputPath.string();
    stbi_write_png(strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);

    return 0;

  }

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }

        static int cameraControllerType = 0;
        const auto cameraControllerTypeChanged = 
          ImGui::RadioButton("Trackball", &cameraControllerType, 0) || ImGui::RadioButton("First Person", &cameraControllerType, 1);
        if(cameraControllerTypeChanged) {
          const auto currentCamera = cameraController->getCamera();
          if(cameraControllerType == 0) {
            cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
          }
          else {
            cameraController = std::make_unique<FirstPersonCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
          }
          cameraController->setCamera(currentCamera);        
        }
      }
      if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
        
        static float theta = 0.f;
        static float phi = 0.f;

        if(ImGui::SliderFloat("theta", &theta, 0, glm::pi<float>()) || ImGui::SliderFloat("phi", &phi, 0, 2.f * glm::pi<float>())) {
          lightDirection = glm::vec3(glm::sin(theta) * glm::cos(phi), glm::cos(theta), glm::sin(theta) * glm::sin(phi));
        }

        static glm::vec3 color(1.f, 1.f, 1.f);
        static float intensity = 1.f;
        if (ImGui::ColorEdit3("color", (float *)&color) || ImGui::InputFloat("intensity", &intensity)) {
          lightIntensity = color * intensity;
        }

        
        ImGui::Checkbox("Light From Camera", &lightFromCam);
        ImGui::Checkbox("apply occlusion", &applyOcclusion);
      }
      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}
